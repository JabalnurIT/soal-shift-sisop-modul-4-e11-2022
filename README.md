# **Laporan Penjelasan Soal Shift Modul 4**
## Sistem Operasi E 2022
### **Kelompok E-11**
**Asisten : Dewangga Dharmawan**  
**Anggota :**
- Jabalnur 5025201241
- Vania Rizky Juliana Wachid 5025201215
- Maula Izza Azizi 5025201104  

## **Daftar Isi Laporan**
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)
- [Kendala](#kendala)

# Soal 1
1A.
Langkah pertama adalah melakukan inialisasi nama directory\\
```c
char typeName[10] = "Animeku_";
```
Selanjutnya membuat fungsi encode dengan pengelompokan saat huruf besar atau huruf kecil. Ketika huruf besar maka akan di-encode menggunakan atbash cipher dan ketika huruf kecil akan di-encode dengan rot13.
Lakukan inialisasi pada fungsi encode\\
Dilakukan checking jika kosong\\
Lalu bedah setiap charnya untuk di encode menggunakan atbash ataupun rot13\\
```c
while (filename[i] != index + 1)
    {
        // Jika ada whitespace, maka continue dan index akan berlanjut
        if(filename[i] == ' ')
        {
            i++;
            continue;
        }

        // Jika Huruf Besar dengan ASCII (65 <= x <= 90)
        if(filename[i] >= 'A' && filename[i] <= 'Z')
        {
            // Di encode menggunakan ALbash Cipher
            for(int j = 0; j < index; j++)
            {
                int start = filename[j];
                filename[j] = countAlphabet - filename[j] - 64;
                filename[j] += 65;
            }
        } else if (filename[i] >= 'a' && filename[i] <= 'z') // Jika huruf kecil dengan ASCII (97 <= x <= 122)
        {
            // Di encode menggunakan rot13
            // Carriage Return = 13 -> ASCII ('CR' = 13)
            if(filename[i] + 13 > 'z')
            {
                filename[i] -= 13;
            } else 
            {
                filename[i] += 13;
            }
        }
        // lanjut ke index selanjutnya
        i++;
    }

```
1B.

Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a
Ketika melakukan rename untuk directory maka harus membaca directory tersebut menggunakan fungsi xmp_readdir sebagai berikut
```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    // Inisialisasi
    int res = 0;
    struct dirent *dir;

    // Mencari string yang diinginkan
    char *letter = strstr(path, typeName), fpath[1000];
    
    // Melakukan pengecekan jika null
    if(letter != NULL)
    {
        decode(letter);
    }

    // Jika string terdapat '/' maka nilai path akan menjadi dirpath
    if(strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    // Inisialisasi
    DIR *dp;
    (void) fi;
    (void) offset;

    // Membuka directory
    dp = opendir(fpath);

    // mengembalikan kode kesalahan terakhir
    if(dp == NULL) 
    {
        return -errno;
    }

    // Membaca directory
    dir = readdir(dp);

    // Jika tidak null
    while (dir != NULL)
    {
        // Inisialisasi
        struct stat st;

        // menetapkan nilai satu byte ke blok memori byte demi byte.
        memset(&st, 0, sizeof(st));
        st.st_ino = dir->d_ino;
        st.st_mode = dir->d_type << 12;

        // Cek jika tidak null
        if(encode != NULL)
        {
            encode(dir->d_name);
        }

        res = filler(buf, dir->d_name, &st, 0);
        if(res != 0 )
        {
            break;
        }
    }

    // Tutup directory
    closedir(dp);
    return 0;
}

```
Ketika melakukan rename untuk file maka harus membaca file yang dibuka menggunakan fungsi xmp_read sebagai berikut
```c
// Membaca data dari file yang dibuka
static int xmp_read (const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    // Inisialisasi
    char fpath[1000];
    int fd, res;

    sprintf(fpath, "%s%s", dirpath, path);

    (void)fi;

    // Membuka path
    fd = open(fpath, O_RDONLY);

    // Mengirimkan error sesuai kode error terakhir
    if(fd == -1)
    {
        return -errno;
    }

    // Untuk membaca file
    res = pread(fd, buf, size, offset);

    // Mengirimkan error sesuai kode error terakhir
    if(res == -1)
    {
        res = -errno;
    }

    // menutup file
    close(fd);
    return res;
}

```
Untuk dapat melakukan rename maka dilakukan dengan fungsi xmp_getattr yaitu dengan tujuan mengubah sesuai dengan typeName yang diinisialisasi
```c
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char newPath[1000], *letter = strstr(path, typeName);

    // Di cek jika letter tidak sama dengan null atau kosong
    if(letter != NULL)
    {
        decode(letter);
    }

    sprintf(newPath, "%s%s", dirpath, path);

    // memberikan informasi rinci tentang file
    res = lstat(newPath, stbuf);

    // mengembalikan kode kesalahan terakhir
    if(res == -1)
    {
        return -errno;
    }
    
    return 0;
}

```
1C.

Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.
Langkah pertama adalah melakukan Inisialisasi\\
Selanjutnya dilakukan checking jika kosong atau NULL\\
Lalu hitung char keseluruhan pada string\\
Dilakukan decode sesuai dengan ketentuan menggunakan atbash cipher dan rot13\\

```c
while (filename[i] != index + 1)
    {
        // Jika ada tanda /, maka akan berhenti
        if(filename[i] == '/')
        {
            break;
        }
        
        // Jika ada tanda ., maka akan berhenti dan index disimpan dengan i yang sekarang
        if(filename[i] == '.')
        {
            index = i;
            break;
        }

        // Jika Huruf Besar dengan ASCII (65 <= x <= 90)
        if(filename[i] >= 'A' && filename[i] <= 'Z')
        {
            // Meng-decode menggunakan ALbash Cipher
            for(int j = 0; j < index; j++)
            {
                int start = name[j] + 1;
                name[j] = countAlphabet - name[j] + 1 - 64;
                name[j] += 65;
            }
        } else if (filename[i] >= 'a' && filename[i] <= 'z') // Jika huruf kecil dengan ASCII (97 <= x <= 122)
        {
            // Meng-decode menggunakan rot13
            // Carriage Return = 13
            if(filename[i] - 13 > 'a')
            {
                filename[i] += 13;
            } else 
            {
                filename[i] -= 13;
            }
        }
        i++;
    }

```
1D

Setiap data yang terencode akan masuk dalam file “Wibu.log”. Contoh isi: RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat. RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba
Untuk membuat log maka menggunakan bantuan dari struct _iobuf FILE dan membuka file tersebut yaitu\\

1E

Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)
Untuk melakukan rekursif pada suatu directory maka dilakukan pengulangan dan pengecekan pada fungsi xmp_readdir. Langkah pertama adalah membaca directory\\
```c
while (dir != NULL)
    {
        // Inisialisasi
        struct stat st;

        // menetapkan nilai satu byte ke blok memori byte demi byte.
        memset(&st, 0, sizeof(st));
        st.st_ino = dir->d_ino;
        st.st_mode = dir->d_type << 12;

        // Cek jika tidak null
        if(encode != NULL)
        {
            encode(dir->d_name);
        }

        res = filler(buf, dir->d_name, &st, 0);
        if(res != 0 )
        {
            break;
        }
    }
``` 





# Soal 2
# Soal 3
